
# coding: utf-8

# In[2]:


import numpy as np
import csv
import pandas as pd
import os
import urllib
import glob
import pathlib


# In[3]:


class Preprocessing:
    
    def __init__(self,csv_file,article_name):
        self.file= pd.read_excel(str(csv_file),sheet_name='Sheet1',usecols=3)
        self.file=self.file.dropna()
        self.file = self.file.astype({"styleId": int})
                                 
        ### Extracting information for required article type ###                         
        self.file=self.file[self.file['ArticleType']==str(article_name)]
                                 
        ### Removing duplicate URLs ###
        self.file = self.file.drop_duplicates(subset=['Links'])
    
    def download_images(self,path):
        df_belts =self.file                         
        df_belts['labels'] = pd.Categorical(pd.factorize(df_belts.Pose)[0])
        df_belts['image_name'] = df_belts.apply(lambda x: str(x['styleId']) + str(x['Pose'])+'0.jpg', axis=1)
        df_belts.reset_index(inplace=True)
        df_belts=df_belts.drop(columns=['index'])

        ### list object to check if same style id and pose is already present ###
        file_name_counter=[]
        ### Create directory if not present along with parent directory ###                        
        pathlib.Path(str(path)).mkdir(parents=True, exist_ok=True)


        for i in range(len(df_belts)):
            csv_row =df_belts.iloc[i]
            img_url = csv_row['Links']
            img_name = csv_row['image_name']
            
            if img_name in file_name_counter:
                img_name=img_name.strip(".jpg")
                img_name=img_name[:-1]+str(int(img_name[-1])+1)+'.jpg'
                df_belts.at[i,'image_name']=img_name
                                 
            file_name_counter.append(img_name)

            url = urllib.parse.unquote(img_url)
            urllib.request.urlretrieve(url, str(path)+img_name)

        print("Download Complete")
        
        files = [os.path.basename(f) for f in glob.glob(path + "**/*.jpg", recursive=True)]
        ### Remove entries for failed image download ###
        data_file=df_belts[df_belts['image_name'].isin(files)]
        data_file.to_csv('belts.csv',index=False)
        ### Updating our dataframe ###
        self.file=data_file                         

    def data_split(self):
        ### Prepare training, validation and test set.
        data_file= pd.read_csv('belts.csv')
        unique_styleid= (data_file.styleId.unique())
        #print(unique_styleid)
        np.random.shuffle(unique_styleid)

        train_id,validation_id,test_id=np.split(unique_styleid, [int(.72 * len(unique_styleid)), int(.86 * len(unique_styleid))])

        len(train_id),len(validation_id),len(test_id)

        train_df=data_file[data_file['styleId'].isin(train_id)]
        validation_df=data_file[data_file['styleId'].isin(validation_id)]
        test_df=data_file[data_file['styleId'].isin(test_id)]

        train_df.to_csv('belts_train.csv',index=False)
        validation_df.to_csv('belts_validation.csv',index=False)
        test_df.to_csv('belts_test.csv',index=False)

        print("Original data frame shape:",data_file.shape)
        print("Training data frame shape:",train_df.shape)
        print("Validation data frame shape:",validation_df.shape)
        print("Test data frame shape:",test_df.shape)
        
        

    


# In[4]:


### Initialize object ###
preprocessor=Preprocessing(csv_file="Pose_Links.xlsx",article_name='Belts')


# In[5]:


preprocessor.download_images(path='/rapid_data/maulik/Pose/Data_Prepare/data_images/Belts/')


# In[6]:


preprocessor.data_split()

