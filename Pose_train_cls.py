
# coding: utf-8

# In[1]:


import torch
import torch.nn as nn
from torch.autograd import Variable
import torchvision.models as models
import numpy as np
import csv
import pandas as pd
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import matplotlib.pyplot as plt
import os
from skimage import io, transform
from torchvision import transforms
#import torchsample as ts
import torch.nn.functional as F
import copy
from torch.optim import lr_scheduler
from torchvision import models

# Ignore warnings
import warnings
warnings.filterwarnings("ignore")

import urllib

plt.ion()   # interactive mode


# In[2]:


from skimage.color import rgb2gray

class QCDataset(Dataset):
    """Fashion QC dataset."""

    def __init__(self, csv_file, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.landmarks_frame = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform
        self.name=self.landmarks_frame.iloc[:, 5].tolist()
        self.pose_name=self.landmarks_frame.iloc[:, 3].tolist()
        self.lbl=self.landmarks_frame.iloc[:, 4].tolist()

    def __len__(self):
        return len(self.landmarks_frame)

    def __getitem__(self, idx):
        #img_name = os.path.join(self.root_dir,str(self.landmarks_frame.iloc[idx, 1]))
        img_name = os.path.join(self.root_dir,self.name[idx])#.strip('.jpg')))
        image = rgb2gray(io.imread(img_name))
       # image = rgb2gray(io.imread(img_name+'.jpg'))
        image=np.expand_dims(image, axis=2)
        
        waist_length=self.lbl[idx]
        #print(img_name,waist_length)
        
        sample = {'image': image, 'labels':waist_length}

        #if self.transform:
         #   sample = self.transform(sample)
        if self.transform:
            sample['image'] = self.transform(sample['image'])
        #return (image,waist_length)
        return sample


# In[3]:


class Rescale(object):
    """Rescale the image in a sample to a given size.

    Args:
        output_size (tuple or int): Desired output size. If tuple, output is
            matched to output_size. If int, smaller of image edges is matched
            to output_size keeping aspect ratio the same.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        self.output_size = output_size

    def __call__(self, sample):
       # image, landmarks = sample['image'], sample['waist_length']
        image=sample
        h, w = image.shape[:2]
        if isinstance(self.output_size, int):
            if h > w:
                new_h, new_w = self.output_size * h / w, self.output_size
            else:
                new_h, new_w = self.output_size, self.output_size * w / h
        else:
            new_h, new_w = self.output_size

        new_h, new_w = int(new_h), int(new_w)

        img = transform.resize(image, (new_h, new_w))

        return img
       # return {'image': img, 'waist_length':landmarks}


# In[4]:


train_tf= transforms.Compose([
            Rescale((224,224)),
            transforms.ToTensor(),
            transforms.Normalize([0.485], [0.229])
        ])



batch_size=8


QC_dataset = QCDataset(csv_file='belts_train.csv', root_dir='data_images/Belts/',transform=train_tf)#Rescale((512,512)))
dataloaders = DataLoader(QC_dataset, batch_size=batch_size,shuffle=True)

QC_dataset_test = QCDataset(csv_file='belts_validation.csv', root_dir='data_images/Belts/',transform=train_tf)#Rescale((512,512)))
dataloaders_test = DataLoader(QC_dataset_test, batch_size=batch_size,shuffle=True)


# In[5]:


######### FineTuning Resnet ###########
num_classes=3

model = models.resnet18(pretrained=True)
num_ftrs = model.fc.in_features
model.fc = nn.Linear(num_ftrs,num_classes)
model.conv1 = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3,
                       bias=False)

optimizer = torch.optim.Adam(list(model.parameters()))#, lr=learning_rate)

# Device configuration
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
print("device is ",device)
model.to(device)

#HyperParameters
num_epochs=20
#learning_rate=0.001

# Loss and optimizer
criterion = nn.CrossEntropyLoss()



# In[7]:


best=0
best1=0

for epoch in range(1,1+num_epochs):
    model.train()
    total_tr_loss=0.
    total=0.
    correct=0.
    
    
    for i,data in enumerate(dataloaders):
        images=Variable(data['image'].float())
        images=images.to(device)
        #images=images.transpose(1,3)
        labels=Variable(data['labels'].long())
        labels=labels.to(device)
        #labels=labels.reshape(-1,1)
        optimizer.zero_grad()
        outputs=model(images)
        
        #outputs=outputs.view(-1)
        
        loss=criterion(outputs,labels)
        
        loss.backward()
        optimizer.step()
        
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum().item()

        
        #losses.append(loss.data[0])
        total_tr_loss+=loss.data
        
        
        if (i+1) % 30 == 0:
            print ('Epoch : %d/%d, Iter : %d/%d,  Loss: %.4f' 
                   %(epoch, num_epochs, i+1, len(QC_dataset)//batch_size, loss.data))
    
    print("######### Training loss for this epoch  ",total_tr_loss)   
    print(" For 1 inch Out of {} examples ,{} correct classified".format(total,correct))
    print("Training accuracy for epoch {} is {} %".format(epoch,correct/total))


    if correct>best:
        best_trainmodel_wts = copy.deepcopy(model.state_dict())
        print("traincopy in epoch "+str(epoch),best,correct)
        best=correct


    
    model.eval()
    correct = 0.
    total = 0.
    
    with torch.no_grad():
        for i,test in enumerate(dataloaders_test):
            images=test['image'].float()
            images=images.to(device)
            labels=Variable(test['labels'].long())
            labels=labels.to(device)
            images = Variable(images.float())
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

            
    print("Total examples in Validation set:",total)
    print("Total examples in Validation set:",correct)
    print("Validation accuracy for epoch {} is {} %".format(epoch,correct/total))

    if correct>best1:
        best_testmodel_wts = copy.deepcopy(model.state_dict())
        print("testcopy in epoch "+str(epoch),best1,correct)
        best1=correct

print('Finished Training')
#torch.save(best_trainmodel_wts, "Belts_trainmodel_resnet18.pth")
#torch.save(best_testmodel_wts, "Belts_testmodel_resnet18.pth")
print('Finished saving model.')


    


# In[8]:


get_ipython().system('pwd')

